package com.comp.mapreduce;

import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class CompDriver {
	public static void main(String[] args) throws Exception {

		Configuration conf = new Configuration();
		conf.set("dfs.client.use.datanode.hostname", "true");
		Job job = Job.getInstance(conf);
		job.setJarByClass(CompDriver.class);
		job.setMapperClass(CompMapper.class);
		job.setReducerClass(CompReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		//String uri = "hdfs://192.168.41.131:8020";
		//Path inputPath = new Path(uri + "/wordcount/input/AComp_Passenger_data_no_error.csv");
		//Path outputPath = new Path(uri + "/wordcount/output/1");
		
		
		FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		
		FileSystem fs = FileSystem.get(new URI(uri), conf);
		fs.delete(outputPath, true);

		FileInputFormat.addInputPath(job, inputPath);
		FileOutputFormat.setOutputPath(job, outputPath);

		job.waitForCompletion(true);

		System.out.println("======Statistical results======");
		FileStatus[] fileStatuses = fs.listStatus(outputPath);
		for (int i = 1; i < fileStatuses.length; i++) {
			System.out.println(fileStatuses[i].getPath());
			FSDataInputStream in = fs.open(fileStatuses[i].getPath());
			IOUtils.copyBytes(in, System.out, 4096, false);
		}
	}
}
