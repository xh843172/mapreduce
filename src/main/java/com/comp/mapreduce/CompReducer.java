package com.comp.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class CompReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
    
    private IntWritable result = new IntWritable();
    private Text passenger_id = new Text();
    private int maxFlightCount = 0;

    public void reduce(Text key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable val : values) {
            sum += val.get();
        }
         
        if (sum > maxFlightCount) {
            maxFlightCount = sum;
            passenger_id.set(key);
        }
    }

    protected void cleanup(Context context) throws IOException, InterruptedException {
        result.set(maxFlightCount);
        context.write(passenger_id, result);
    }
 
}
