package com.comp.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class CompMapper extends Mapper<Object, Text, Text, IntWritable> {
	private Text text = new Text();
    private IntWritable outV = new IntWritable(1);

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
    	String values[] = value.toString().split(",");
        text.set(values[0]);
        context.write(text, outV);
    }
}
